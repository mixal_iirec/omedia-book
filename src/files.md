# File format
This file format are used for configuration, and for some input to allow more general text editing.

## Comments
Comments are marked by first symbol of line being `#`.
```
#This is comment
```

## Assignment
`value` can contain anything but newline. `var` cannot contain newline, `=` and prefix/suffix whitespace is trimmed.
```
var=value
```

## Subvalues
`var` cannot contain newline, `=` and prefix/suffix whitespace is trimmed.
```
var{
	...
}
```

## List
Can be used to add multiple channels etc.:
```
{
	name=channel1
	url=
}
{
	name=channel2
	url=
}
```
