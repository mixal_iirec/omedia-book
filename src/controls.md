# Controls
List of all controls is accessible with `<F1>` or `help`.

## Special Actions
Actions without specified target.

- `ch` temporal custom playlist, creates short term playlist with items filtered from previously displayed playlist
- `sort/`, `sort?` take `date` or name of integer attribute as modifier
- `draw/` [quick drawer](drawer.md#quick-drawer)
- `Paste shallow` pastes only to the current list displayed to the user
- `Paste deep` creates long term copy in given channel
- `enum` shows line numbers in given list, useful for `Goto` movement
- `f` fetch new items - can ignore items released before last fetched item
- `F` fetch all items - checks for all items released by given channel that are not yet fetched

## Movement
Can be prefixed by number of repeats.

- `/`, `?` searches with regex as modifier
	- when used with range action, it is applied only to matching items
- all other movements when used with range action, apply the action on all items between starting position and ending position of the movement

## Range Actions
Used in combination with movement (range action first) to specify targets.

- `Delete deep` deletes from long term memory (is not `cut`)
- `Delete shallow` deletes only from current list displayed to the user (is not `cut`)
- `set/`, `set?` should be followed by name of bool to set

## Modifiers
Modifiers `/` and `?` are followed by any input - modifier - until `<Enter>`.
