# Playlist
Automatically generated list of items based on filter matching of any attributes. For any item all attributes must match to be included in the playlist.

- `name` is only for user
- `date` matches list of dates / date ranges separated by `;` (`YYYY-MM-DD`) eg. `2020-02-01;2020-03-01..2020-04-01`
- vars in `i32` matches list of ints / int ranges separated by `;` eg. `0;10..20`
- vars in `strings` matches by regex
- vars in `bools` matches either `true` or `false`

