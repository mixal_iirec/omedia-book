# Config

`config.cfg` can contain following configuration:

- `dir` data directory
- `editor` preferred editor
- `multiple_visible_tabs` whether tabs should be visible side by side, or stacked
- `enumerate_lists` whether to enumerate list by default
- `subtitle_languages` list of language codes to download subtitles for
- `scrape_threads` number of threads in threadpool, used by scrapers
- `scrape_item_buffer` after how many scraped items to send them, so that it is possible to quit mid of scraping without losing all the work
- `max_resolution_stream` maximal vertical resolution of streaming, if possible
- `max_resolution_download` maximal vertical resolution of download, if possible
- `bools` can specify names of 8..32 bool attributes:
```
bools{
	8=name
	9=name2
}
```
useful for playlists that cannot be conveniently matched from other attributes
