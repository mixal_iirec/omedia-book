# Scapers

Scrapers download relevant info or video/audio from given url. They should be written in python3.

Directories in `./scrapers/` each correspond to one scraper.
`./scrapers/priority_match` is used to find `scaper_dir` if not specified by user, first url match (regex) gives the scraper.

## scrape_channel.py
```
def scrape_channel(url: str, from_date: str):
```
- `from_date`: `"YYYYMMDD"` any older item can be ignored

Should return array of dictionaries (as in `scrape_item.py`) all dictionaries must contain the same keys.

If scraper does not include `scraper_item.py`, these dictionaries are used, otherwise only key `"url"` from each dictionary is taken and used for `scrape_item.py`.

## scrape_item.py
```
def scrape_item(url: str):
```
Should return dictionary of strings and ints which will be used as attributes. Key `"date"` should correspond to string of format `YYYYMMDD`.

## download.py
```
def download(type: str, url: str, dir: str, max_resolution: int, langs: str):
```
- `type`: `"media_only"` or `"media_and_subtitles"` or `"subtitles_only"`
- `dir` where to save downloaded files
- `max_resolution` maximal vertical resolution, if possible
- `langs` language codes separated by space (0x20)

Should return tuple `(filename: str, subtitle_files: [str])`
- `filename` filename of downloaded media in `dir`
- `subtitle_files` array of filenames of subtitles in `dir`, all must end with `.vtt`

Both can be `None`

`download.py` can print print `done/from` to stdout (is set to unbuffered) to report progress, where `done` and `from` are ints.
