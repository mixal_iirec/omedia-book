# Channel

Collection of items, probably with url where to find new items.

- `name` is only for user
- `url` specifies source of new items, might not be specified if items will be added manually
- `scraper_dir` if not specified it is automatically found, see [scrapers](./scrapers.md)
