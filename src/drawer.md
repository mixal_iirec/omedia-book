# Drawer

Specify how Channel/Playlist/Item should be displayed in list based on its attributes. Can span over several lines, specify width of attributes.

`./drawers/*` contains drawers to choose from.

## General format
```
name=Name

#list of separate lines
{
	#how many lines should these attributes occupy
	lines=1

	#list of attributes
	{
		align=`left` OR `center` OR `right`, `left` by default

		width{
			type=`proportional` OR `absolute`, `absolute` by default
			size=`1` by default
		}

		property{
			type=`name` OR `date` OR `channel` OR `pipe`, `blank` by default
		} OR
		property{
			type=`text`
			value=Random text to display
		} OR
		property{
			type=`string` OR `i32` OR `bool`
			name= name of attribute
		}
	}
}
```


## Quick drawer
`draw/` can be used to specify single-line drawer, names of attributes to be displayed are separated by pipe:
`draw/title|duration|channel`
