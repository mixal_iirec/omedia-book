# Item

Corresponds to single video/audio, should contain `url` or `downfile` where to find the video/audio.
Attributes:
- `date` `YYYY-MM-DD`
- `bools` either true or false
- `i32` 32bit signed integer, `0` if not specified
- `strings` `""` if not specified

All names of attributes must be allowed names of files.
