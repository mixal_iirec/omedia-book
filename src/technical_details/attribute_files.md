# Attribute files

Attribute file contains all and only information related to given named attribute of all Items in given Channel.

Each channel has its own attribute files, so it is trivial to remove channels and because most string editing makes sense only for one channel at a time.

## Constants sized types
Those include integers, dates and 32 bool flags.

The attribute file can be thought	of as growable vector, where file size is used to get number of items.

## Variable sized types
Currently only strings.

Similar to constant sized types however offsets of each string is given by another file of u32ints.

## Benefits
- files do not waste any space and do not need any complex parsing
- easily addable/removable new attribute names
- most string attributes are not needed, so it is possible to quickly read only needed attributes

## Problems
- files of constants sized types ares often smaller than 1 page (4KiB), thus wasting space and making loading longer
