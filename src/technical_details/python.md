# Python

Scrapers are executed using `python3`.
Scrapers are designed as functions to be called by internal interpreter\*. However because of python GIL, it is problematic to parallelize scrapers.

Because both [rust-cpython](https://github.com/dgrunwald/rust-cpython) and [pyo3](https://github.com/PyO3/pyo3) don't allow multiple instances of python, currently all scrapers are run as one-time-use python3 child process using [Command](https://doc.rust-lang.org/std/process/struct.Command.html).

Communicating into python is done using command's arguments, its output is formatted with appended python code with following formatting:
- `item_scraper.py` output dictionary is formatted as `Skey=value\n` where I/S marks int/string
- `channel_scraper.py` for each item the same as `item_scraper.py`, all items must have the same keys of attributes, so no attribute is badly assigned when processing
- `download.py` all subtitle files are printed first, video/audio file is printed last



\* only downloader reports progress to stdout

