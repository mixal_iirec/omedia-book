# Unicode

String attributes can contain any valid unicode, only `\n` is currently replaced by `\t\t`(to preserve the information of `\n`) and displayed as `  ` (two spaces), mainly because of editing which cannot have `\n` as part of input.

It should draw any unicode string supported by given terminal, however wrapping onto new line is problematic.

Currently it is assumed that all graphemes are 1 space wide, this is fine for latin, cyrilic, hebrew, emojis, but chinese, japanese and hangul all use 2 space wide graphemes.
