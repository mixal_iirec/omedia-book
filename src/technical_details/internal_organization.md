# Internal organization

## Thread Communication
All communication between threads is done via [channels](https://doc.rust-lang.org/std/sync/mpsc/index.html) with messages defined in `msg.rs`.
Only exception is `entries.rs` which uses `Mutex` for assigning ids to attribute names.

## Threads
- `UI` (`ui/`) shall have exclusive access to the user input and output, uses [pancurses](https://github.com/ihalila/pancurses)
	- `key_mapper.rs` maps input into actions using trie
	- `handler/` stores everything related to `UI`, handles messages and actions
	- `list/` defines and implements trait `List` which draws its content, handles updates and list specific actions
	- `drawers/` customizable drawers for lists of `Item`s, `Playlist`s and `Channel`s
	- `view.rs` expands `pancurses::Window`
	- `variable_editor.rs` creates temporal file to be edited by user
- `backend` shall have exclusive access to all data files but video/audio, subtitles, shall take care of all data manipulation
	- `backend/` takes care of incoming requests, mostly by separating the request for each channel
	- `channel/` holds `Item`s and interacts with attribute files
	- `item/{changer/, changer_conditional/, matcher/}` changing and filtering of `Item`s
	- `downloader.rs` queue of downloads and deployment of downloader threads
- `scrapers/download_scraper.rs` downloads video/audio, subtitles for given item URL
- `scrapers/{channel,item}_scraper.rs` check for and fetch new `Item`'s attributes from given channel URL
- `player/` plays the video/audio using mpv and handles its input

Only threads `UI` and `backend` (+threadpool) are run on start, all other threads are started as needed.

## Miscellaneous
- `config.rs` Configuration loaded on start and distributed to all threads
- `files/` work with filesystem, compact(attribute) files, the user facing file format
- `error.rs` definition of `Error` and `Result`
- `item/mod.rs` / `channel/channel_info.rs` / `playlist/playlist_info.rs` info used by both `UI` and `backend` however it is read-only for `UI`
- `general/` most notably implements `Id`, `IdMap` and `IdSet` which are represented as `Vec` with assumption that all `Id`s used as indices to access the `Vec` are small and mostly continuous so they reduce memory usage and give `O(1)` time complexity of access
