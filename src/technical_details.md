# Technical details

This is some information mostly useful for someone interested in modifying parts of the code.


## Backend

Omedia is centered around Items, each Item consists of named string/int/bool attributes, eg. strings `title` or `url`, int `duration` and bool `seen`.

Items from the same source are grouped under one Channel. Channels are isolated from each other and each Channel holds its Items as [Attribute Files](./technical_details/attribute_files.md) while keeping loaded minimal copy without string attributes to reduce RAM usage.

Each channel can have assigned [scrapers](./scrapers.md), those are [python](./technical_details/python.md) scripts which describe how to find new Items from given source (channel scraper), how to find each Item's attributes (item scraper) and how to download video/audio/subtitles of given Item (download).

Playlists are specified by Item filters - whitelist - of Items. They can be either global directly under Backend Handler to filter from all Items or under individual Channel to filter only from Items of given Channel.

Backend Handler groups all Channels together and distributes operations over Items to Channels based on each Item's `ChannelId`, so from the outside operation over any set of Items can be sent to the Handler. Similarly are distributed operations over Playlists or directly Channels.

Backend thread listens for requests from other threads, from scraper threads new Items to add, from download thread edits to downloaded Items what are the names of downloaded files, from player thread to set `seen` attribute, and from UI thread what is needed for displaying.
Backend then gives the request to the Handler and possibly sends new info to UI thread.

## UI
Terminal output is done using [pancurses](https://crates.io/crates/pancurses) and current implementation has problem with wrapping [wide unicode graphemes](./technical_details/unicode.md).
Communication with `mpv` is done on separate thread using [mpv-rs](https://crates.io/crates/mpv).

User input is mapped into actions using trie.

Most of user output is displayed using tabs which are implemented as list of items (Items, Playlists, Channels, Progress Bars, Drawers...). All list are derived from BasicList which implements movement and partially display drawing, while each list supplies its own content to draw, search in, hadles updates sent from backend and handles user actions which are specific to the list type.
And specially Item, Playlist and Channel lists use customizable [drawers](./drawer.md).


UI Handler contains all current lists, sends them updates from backend, and handles actions not specific to individual list types.
Handler also contains everything related to UI, most notably keymapper from input to actions, and progress bars.

UI thread listens for updates from backend and for user input to be sent to the Handler, which might send new requests to the backend.
