# Omedia

Omedia offers a simple TUI interface for watching/listening to any publicly accessible video/audio with supplied scraper (RSS or youtube-dl by default) using mpv.



## Features
- checking for new videos/audios, without relying on official subscription methods which might filter content
- downloading video/audio (subtitles included)
- tracking 'seen' attribute (set if user has seen 75 % of video/audio)
- playlists filtering by any attribute eg. title, duration, subtitle content, 'seen'
- string attributes lazily loaded
- editing of attributes

## Problems
- no syncing across multiple devices, must be done externally eg. [syncthing](https://syncthing.net/)
- restricted videos (age restricted, paywalled) might require supplying login information / cookies to the scraper (in python)

## Requirements
- `mpv`
- `python3` (scrapers)
- python3 `youtube-dl` (youtube-dl scraper)
- python3 `feedparser` (RSS scraper)
